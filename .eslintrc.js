module.exports = {
  extends: ['react-app', 'prettier', 'prettier/@typescript-eslint', 'prettier/react'],

  plugins: ['@typescript-eslint', 'import', 'jsx-a11y', 'react', 'react-hooks', 'prettier'],
};
