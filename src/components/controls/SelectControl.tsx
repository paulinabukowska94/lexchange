import React, { FunctionComponent } from 'react';
import Select from 'react-select';
import './controls.scss';

interface SelectControlProps {
  options: any;
  isMulti?: boolean;
  placeholder: string;
  noOptionsMessage: any;
  onChange?: any;
  ref?: any;
  name?: string;
  value?: any;
}

export const SelectControl: FunctionComponent<SelectControlProps> = (props) => {
  return (
    <div className="control select">
      <Select {...props} />
    </div>
  );
};
