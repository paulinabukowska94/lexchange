import React, { FunctionComponent } from 'react';
import { NotificationsActive, Assignment, Person, Home, PostAdd } from '@material-ui/icons';

export const NotificationIcon: FunctionComponent = () => {
  return <NotificationsActive />;
};

export const TasksIcon: FunctionComponent = () => {
  return <Assignment />;
};

export const MyAccountIcon: FunctionComponent = () => {
  return <Person />;
};

export const HomeIcon: FunctionComponent = () => {
  return <Home />;
};

export const AddTaskIcon: FunctionComponent = () => {
  return <PostAdd />;
};
