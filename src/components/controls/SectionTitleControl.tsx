import React, { FunctionComponent } from 'react';
import Typography from '@material-ui/core/Typography';
import './controls.scss';

interface SectionTitleControlProps {
  text: string;
  startIcon?: any;
  endIcon?: any;
}

export const SectionTitleControl: FunctionComponent<SectionTitleControlProps> = (props) => {
  return (
    <div className="control label">
      <Typography variant="subtitle2">
        <div>
          {props.startIcon} <p className="label-text">{props.text.toUpperCase()}</p> {props.endIcon}
        </div>
      </Typography>
    </div>
  );
};
