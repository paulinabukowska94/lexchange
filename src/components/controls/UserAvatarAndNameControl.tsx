import React, { FunctionComponent } from 'react';
import { ListItem, ListItemAvatar, Avatar, ListItemText } from '@material-ui/core';
import example from './user.png';

interface UserAvatarAndNameControlProps {
  img?: string;
  title?: string;
  subtitle?: string;
}

export const UserAvatarAndNameControl: FunctionComponent<UserAvatarAndNameControlProps> = ({
  img,
  title,
  subtitle,
}) => {
  return (
    <ListItem alignItems="flex-start">
      <ListItemAvatar>
        <Avatar alt="Task" src={img || example} />
      </ListItemAvatar>
      <ListItemText primary={title} secondary={subtitle} />
    </ListItem>
  );
};
