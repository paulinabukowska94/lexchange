import React, { FunctionComponent } from 'react';
import Button from '@material-ui/core/Button';
import './controls.scss';

interface ButtonControlProps {
  onClick?: any;
  color?: any;
  variant?: any;
  startIcon?: any;
  endIcon?: any;
  size?: any;
  fullWidth?: boolean;
  type?: any;
  className?: any;
}

export const ButtonControl: FunctionComponent<ButtonControlProps> = (props) => {
  return (
    <div className="control button">
      <Button
        {...props}
        color={props.color || 'primary'}
        variant={props.variant || 'contained'}
        fullWidth={props.fullWidth || false}
        className={`button ${props.className}`}
      >
        {props.children}
      </Button>
    </div>
  );
};
