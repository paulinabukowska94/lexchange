import React, { FunctionComponent } from 'react';
import Switch from '@material-ui/core/Switch';
import './controls.scss';

interface SwitchControlProps {
  checked: any;
  options?: { id: number; name: string }[];
  onChange?: any;
}

export const SwitchControl: FunctionComponent<SwitchControlProps> = (props) => {
  const [checked, setChecked] = React.useState(props.checked);

  const toggleChecked = (e: any) => {
    setChecked(() => (e.target ? e.target.checked : false));
    props.onChange(e.target.checked);
  };

  return (
    <div className="control switch">
      {props.options && props.options[0] && <p className="option">{props.options[0].name}</p>}
      <div className="option">
        <Switch
          {...props}
          checked={checked}
          onChange={toggleChecked}
          color="primary"
          inputProps={{ 'aria-label': 'secondary checkbox' }}
        />
      </div>
      {props.options && props.options[1] && <p className="option">{props.options[1].name}</p>}
    </div>
  );
};
