import React, { FunctionComponent } from 'react';
import Typography from '@material-ui/core/Typography';
import './controls.scss';

interface LabelControlProps {
  text?: string | number | Date;
  label?: string;
  variant?: any;
  startIcon?: any;
  endIcon?: any;
}

export const LabelControl: FunctionComponent<LabelControlProps> = (props) => {
  return (
    <div className="control label">
      {props.label && (
        <Typography variant="body2">
          <span className="top-label">{props.label}</span>
        </Typography>
      )}
      {props.text && (
        <Typography variant={props.variant || 'body1'}>
          <>
            {props.startIcon} <span className="label-text">{props.text}</span> {props.endIcon}
          </>
        </Typography>
      )}
    </div>
  );
};
