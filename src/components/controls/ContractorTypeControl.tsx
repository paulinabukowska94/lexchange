import React, { FunctionComponent } from 'react';
import { IType } from '../../models/api/interfaces';
import { Avatar, Chip } from '@material-ui/core';

export const ContractorTypeControl: FunctionComponent<{ type: IType }> = ({ type }) => {
  return (
    <div className="">
      <Chip
        color="primary"
        avatar={<Avatar>{type.id}</Avatar>}
        label={type.name}
        variant="outlined"
      />
    </div>
  );
};
