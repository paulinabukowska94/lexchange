import React, { FunctionComponent } from 'react';
import Typography from '@material-ui/core/Typography';
import './controls.scss';

interface PageTitleControlProps {
  text: string;
  startIcon?: any;
  endIcon?: any;
}

export const PageTitleControl: FunctionComponent<PageTitleControlProps> = (props) => {
  return (
    <div className="control label">
      <Typography variant="h6">
        <div>
          {props.startIcon} <p className="label-text">{props.text.toUpperCase()}</p> {props.endIcon}
        </div>
      </Typography>
    </div>
  );
};
