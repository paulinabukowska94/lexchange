import React, { FunctionComponent } from 'react';
import Modal from 'react-modal';

import './controls.scss';

interface ModalProps {
  show: boolean;
  onClose?: () => any;
}

export const ModalControl: FunctionComponent<ModalProps> = (props) => {
  const customStyles = {
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
    },
  };

  return (
    <Modal isOpen={props.show} style={customStyles}>
      {props.children}
    </Modal>
  );
};
