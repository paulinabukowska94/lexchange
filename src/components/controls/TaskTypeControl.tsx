import React, { FunctionComponent } from 'react';
import { LabelControl } from './LabelControl';
import { IType } from '../../models/api/interfaces';

export const TaskTypeControl: FunctionComponent<{ type: IType }> = ({ type }) => {
  return type && <LabelControl text={`${type.id} ${type.name}`} />;
};
