import React, { FunctionComponent } from 'react';
import './controls.scss';
import { Button } from '@material-ui/core';
import { KeyboardArrowLeft } from '@material-ui/icons';

export const BackButton: FunctionComponent = () => {
  return (
    <Button onClick={() => window.history.go(-1)}>
      <KeyboardArrowLeft />
      Wróć
    </Button>
  );
};
