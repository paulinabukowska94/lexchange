import React, { FunctionComponent } from 'react';
import './controls.scss';

interface NoContentControlProps {
  text: string;
}

export const NoContentControl: FunctionComponent<NoContentControlProps> = (props) => {
  return <div>{props.text}</div>;
};
