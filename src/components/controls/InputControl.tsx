import React, { FunctionComponent } from 'react';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputAdornment from '@material-ui/core/InputAdornment';
import './controls.scss';

interface InputControlProps {
  onChange?: any;
  prefix?: any;
  suffix?: any;
  multiline?: boolean;
  initialValue?: any;
  inputRef?: any;
  name?: string;
  error?: boolean;
  type?: any;
}

export const InputControl: FunctionComponent<InputControlProps> = (props) => {
  return (
    <div className="control input">
      <OutlinedInput
        {...props}
        labelWidth={0}
        startAdornment={<InputAdornment position="start">{props.prefix}</InputAdornment>}
        endAdornment={<InputAdornment position="end">{props.suffix}</InputAdornment>}
        rows="4"
        margin="dense"
        defaultValue={props.initialValue || ''}
      />
    </div>
  );
};
