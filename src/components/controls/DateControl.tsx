import React, { FunctionComponent, useState } from 'react';
import './controls.scss';
import { SingleDatePicker } from 'react-dates';
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';

interface DateControlProps {
  id: string;
  date: any;
  onChange: any;
}

export const DateControl: FunctionComponent<DateControlProps> = (props) => {
  const [focused, setFocused] = useState<boolean | null>(false);

  return (
    <div className="control date">
      <SingleDatePicker
        id="a"
        numberOfMonths={1}
        onDateChange={(date) => props.onChange({ target: { value: date } })}
        onFocusChange={({ focused }) => setFocused(focused)}
        // FIXME: change to `focused` instead of `!!focused` after typescript & @types/react upgrade - there will be overload for init value
        focused={!!focused}
        date={props.date}
        displayFormat="DD-MM-YYYY"
        hideKeyboardShortcutsPanel
        withFullScreenPortal={window.innerWidth < 400}
      />
    </div>
  );
};
