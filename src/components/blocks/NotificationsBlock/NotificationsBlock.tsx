import React, { FunctionComponent } from 'react';
import { INotification } from '../../../models/api/interfaces';
import { NotificationListItem } from '../../shared/NotificationListItem/NotificationListItem';
import { A } from 'hookrouter';
import { SectionTitleControl } from '../../controls/SectionTitleControl';
import { NoContentControl } from '../../controls/NoContentControl';
import { Spinner } from '../../shared/LayoutItems/Spinner';
import { ButtonControl } from '../../controls/ButtonControl';
import { NotificationIcon } from '../../controls/Icons';

export const NotificationsBlock: FunctionComponent<{ items: INotification[] }> = ({ items }) => {
  return (
    <div className="block notifications-block">
      <SectionTitleControl
        startIcon={<NotificationIcon />}
        text={`Powiadomienia (${items ? items.length : 0})`}
      />
      {items && items.length === 0 ? (
        <NoContentControl text="Brak powiadomień." />
      ) : items && items.length > 0 ? (
        <>
          {items.slice(0, 3).map((t: INotification) => (
            <NotificationListItem key={t.id} item={t} />
          ))}
          <A href="/">
            <ButtonControl variant="outlined">Zobacz wszystkie...</ButtonControl>
          </A>
        </>
      ) : (
        <Spinner />
      )}
    </div>
  );
};
