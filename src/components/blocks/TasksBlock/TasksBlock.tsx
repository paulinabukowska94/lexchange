import React, { FunctionComponent } from 'react';
import { ITask } from '../../../models/api/interfaces';
import { TaskListItem } from '../../shared/TaskListItem/TaskListItem';
import { A } from 'hookrouter';
import { Spinner } from '../../shared/LayoutItems/Spinner';
import { SectionTitleControl } from '../../controls/SectionTitleControl';
import { NoContentControl } from '../../controls/NoContentControl';
import { ButtonControl } from '../../controls/ButtonControl';
import { TasksIcon } from '../../controls/Icons';

interface TasksBlockProps {
  items: ITask[];
  title: string;
  type: string;
}

export const TasksBlock: FunctionComponent<TasksBlockProps> = ({ items, title, type }) => {
  return (
    <div className="block tasks-block">
      <SectionTitleControl
        startIcon={<TasksIcon />}
        text={`${title} (${items ? items.length : 0})`}
      />
      {items && items.length === 0 ? (
        <NoContentControl text="Brak zleceń." />
      ) : items && items.length > 0 ? (
        <>
          {items.slice(0, 3).map((t: ITask) => (
            <TaskListItem key={t.id} item={t} />
          ))}
          <A href={`/tasks/${type}`}>
            <ButtonControl variant="outlined"> Zobacz wszystkie...</ButtonControl>
          </A>
        </>
      ) : (
        <Spinner />
      )}
    </div>
  );
};
