import React, { FunctionComponent } from 'react';
import { CircularProgress, Box } from '@material-ui/core';

export const Spinner: FunctionComponent = () => {
  return (
    <Box p={3}>
      <CircularProgress />
    </Box>
  );
};
