import React, { FunctionComponent } from 'react';
import { A } from 'hookrouter';
import logo from '../../../assets/logo_full_color.svg';

export const LayoutLogo: FunctionComponent = () => {
  return (
    <A href="/">
      <img src={logo} className="logo" alt="logo" />
    </A>
  );
};
