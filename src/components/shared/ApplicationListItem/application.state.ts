import { useState } from 'react';
import { IApplication } from '../../../models/api/interfaces';

export const useApplicationModel = (item: IApplication) => {
  const [application] = useState<IApplication>({ ...item });

  return { application };
};
