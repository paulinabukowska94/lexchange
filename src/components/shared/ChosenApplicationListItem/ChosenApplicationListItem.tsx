import React, { FunctionComponent } from 'react';
import { IApplication } from '../../../models/api/interfaces';
import { useApplicationModel } from './application.state';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import img from './application.png';
import { ButtonControl } from '../../controls/ButtonControl';

export const ChosenApplicationListItem: FunctionComponent<{
  item: IApplication;
  visitProfile: any;
}> = ({ item, visitProfile }) => {
  const { application } = useApplicationModel(item);

  return (
    <React.Fragment>
      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Avatar alt="Task" src={img} />
        </ListItemAvatar>
        <ListItemText
          primary={application.contractor.email}
          secondary={application.price ? `Cena: ${application.price}` : 'Brak ceny'}
        />
        <ButtonControl variant="outlined" onClick={() => visitProfile(application.contractor.id)}>
          Zobacz profil
        </ButtonControl>
      </ListItem>
    </React.Fragment>
  );
};
