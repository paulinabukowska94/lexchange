import { useState } from 'react';
import { ITask } from '../../../models/api/interfaces';

export const useTaskModel = (item: ITask) => {
  const [task] = useState<ITask>({ ...item });

  return { task };
};
