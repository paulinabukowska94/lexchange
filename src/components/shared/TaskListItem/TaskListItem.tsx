import React, { FunctionComponent } from 'react';
import { ITask } from '../../../models/api/interfaces';
import { useTaskModel } from './task.state';
import { A } from 'hookrouter';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import img from './task.png';
import { ButtonControl } from '../../controls/ButtonControl';

export const TaskListItem: FunctionComponent<{ item: ITask }> = ({ item }) => {
  const { task } = useTaskModel(item);

  return (
    <React.Fragment>
      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Avatar alt="Task" src={img} />
        </ListItemAvatar>
        <ListItemText primary={task.title} secondary={task.description} />
        <A href={`/task/${item.id}`}>
          <ButtonControl>Szczegóły</ButtonControl>
        </A>
      </ListItem>
    </React.Fragment>
  );
};
