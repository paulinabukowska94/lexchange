import { ITask } from './../../models/api/interfaces';

export const findTaskById = (taskId: number, mine: ITask[], all: ITask[]) => {
  const merged = mine.concat(all);
  const unique = merged.filter((item: any, pos: any) => merged.indexOf(item) === pos);
  let taskById = unique.find((t: ITask) => t.id === taskId);
  return taskById;
};
