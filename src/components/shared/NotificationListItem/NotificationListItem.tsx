import React, { FunctionComponent } from 'react';
import { INotification } from '../../../models/api/interfaces';
import { useNotificationModel } from './notification.state';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import img from './task.png';
import { ButtonControl } from '../../controls/ButtonControl';

export const NotificationListItem: FunctionComponent<{ item: INotification }> = ({ item }) => {
  const {
    notification,
    onYesNoNotificationClick,
    onStatementNotificationClick,
  } = useNotificationModel(item);

  return (
    <React.Fragment>
      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Avatar alt="Task" src={img} />
        </ListItemAvatar>
        <ListItemText primary={notification.message} secondary={notification.message} />
        {
          //item.type === NotificationType.YESNO &&
          <div>
            <ButtonControl onClick={(e: any) => onYesNoNotificationClick(e, true)}>
              Tak
            </ButtonControl>
            <ButtonControl onClick={(e: any) => onYesNoNotificationClick(e, false)}>
              Nie
            </ButtonControl>
          </div>
        }
        {
          //item.type === NotificationType.STATEMENT &&
          <div>
            <ButtonControl onClick={onStatementNotificationClick}>Ok, rozumiem</ButtonControl>
          </div>
        }
      </ListItem>
    </React.Fragment>
  );
};
