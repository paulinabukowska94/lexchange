import { useState } from 'react';
import { INotification } from '../../../models/api/interfaces';

export const useNotificationModel = (item: INotification) => {
  const [notification] = useState<INotification>({ ...item });

  const onYesNoNotificationClick = (e: any, choice: boolean) => {
    //notify API about the choice and mark notification as read
  };

  const onStatementNotificationClick = () => {
    //mark notification as read
  };

  return { notification, onYesNoNotificationClick, onStatementNotificationClick };
};
