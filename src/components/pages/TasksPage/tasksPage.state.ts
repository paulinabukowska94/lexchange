import { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { TasksState } from '../../../models/api/tasks';
import { ITask } from '../../../models/api/interfaces';

export const useTasksPageModel = (type: string) => {
  const tasks = useSelector((state: { tasks: TasksState }) => state.tasks);
  const [items, setItems] = useState<ITask[] | null>();

  useEffect(() => {
    switch (type) {
      case 'mine':
        setItems(tasks.mine);
        break;
      case 'others':
        setItems(tasks.others);
        break;
      case 'all':
        setItems(tasks.all);
        break;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [type]);

  return { items };
};
