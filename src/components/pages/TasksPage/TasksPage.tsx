import React, { FunctionComponent } from 'react';
import { useTasksPageModel } from './tasksPage.state';
import { ITask } from '../../../models/api/interfaces';
import { TaskListItem } from '../../shared/TaskListItem/TaskListItem';
import { PageTitleControl } from '../../controls/PageTitleControl';
import { NoContentControl } from '../../controls/NoContentControl';
import { Spinner } from '../../shared/LayoutItems/Spinner';
import { BackButton } from '../../controls/BackButton';

interface TasksPageProps {
  type: string;
}

export const TasksPage: FunctionComponent<TasksPageProps> = ({ type }) => {
  const { items } = useTasksPageModel(type);

  return (
    <div className="page tasks-page">
      <BackButton />
      <PageTitleControl text="Zlecenia" />
      {items && items.length === 0 ? (
        <NoContentControl text="Brak zleceń." />
      ) : items && items.length > 0 ? (
        <>
          {items.map((t: ITask) => (
            <TaskListItem key={t.id} item={t} />
          ))}
        </>
      ) : (
        <Spinner />
      )}
    </div>
  );
};
