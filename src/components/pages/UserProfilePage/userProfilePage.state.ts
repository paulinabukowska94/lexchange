import { useState, useEffect } from 'react';
import { IUser } from '../../../models/api/interfaces';
import Api from '../../../models/api/api';

export const useUserProfilePageModel = (id: any) => {
  const [user, setUser] = useState({} as IUser);

  useEffect(() => {
    Api.getWithAuthorization(`user/profile?id=${id}`).then((res: any) => {
      setUser(res.data);
    });
  }, [id]);

  return { user };
};
