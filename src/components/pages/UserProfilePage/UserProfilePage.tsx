import React, { FunctionComponent } from 'react';
import { useUserProfilePageModel } from './userProfilePage.state';
import { LabelControl } from '../../controls/LabelControl';
import { PageTitleControl } from '../../controls/PageTitleControl';
import { MyAccountIcon } from '../../controls/Icons';
import { IType } from '../../../models/api/interfaces';
import { ContractorTypeControl } from '../../controls/ContractorTypeControl';
import { BackButton } from '../../controls/BackButton';

export const UserProfilePage: FunctionComponent<{ id: any }> = ({ id }) => {
  const { user } = useUserProfilePageModel(id);

  return (
    <div className="page user-profile-page">
      <BackButton />
      <PageTitleControl text="Profil" />
      {user && (
        <>
          <LabelControl text={user.email} startIcon={<MyAccountIcon />} variant="h6" />
          {user.type &&
            user.type.map((t: IType, i: any) => <ContractorTypeControl key={i} type={t} />)}
        </>
      )}
    </div>
  );
};
