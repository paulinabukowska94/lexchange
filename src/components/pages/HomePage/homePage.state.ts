import { useState, useEffect } from 'react';
import { ITask, INotification } from '../../../models/api/interfaces';
import { useSelector } from 'react-redux';

export const useHomePageModel = () => {
  const [myTasks, setMyTasks] = useState<ITask[]>([]);
  const [othersTasks, setOthersTasks] = useState<ITask[]>([]);
  const [notifications, setNotifications] = useState<INotification[]>([]);

  const tasks = useSelector((state: any) => state.tasks);

  useEffect(() => {
    setMyTasks(tasks.mine);
    setOthersTasks(tasks.others);
    setNotifications([]);
  }, [tasks]);

  return { notifications, myTasks, othersTasks };
};
