import React, { FunctionComponent } from 'react';
import { NotificationsBlock } from '../../blocks/NotificationsBlock/NotificationsBlock';
import { TasksBlock } from '../../blocks/TasksBlock/TasksBlock';
import { useHomePageModel } from './homePage.state';
import { PageTitleControl } from '../../controls/PageTitleControl';

export const HomePage: FunctionComponent = () => {
  const { notifications, myTasks, othersTasks } = useHomePageModel();

  return (
    <div className="page home-page">
      <PageTitleControl text="Start" />
      <NotificationsBlock items={notifications} />
      <TasksBlock title="Moje zlecenia" items={myTasks} type="mine" />
      <TasksBlock title="Szukaj zleceń" items={othersTasks} type="others" />
    </div>
  );
};
