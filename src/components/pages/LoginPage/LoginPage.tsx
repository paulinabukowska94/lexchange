import React, { FunctionComponent } from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { userLoginAsync, userFillCredentials } from '../../../redux/actions/user';
import { Credentials } from '../../../models/api/user';
import { ButtonControl } from '../../controls/ButtonControl';

export const LoginPage: FunctionComponent = () => {
  // FIXME: needs auto login for dev that would also work with production
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const user = useSelector((state: any) => state.user);
  const dispatch = useDispatch();

  const login = (credentials: Credentials) => {
    dispatch(userFillCredentials(credentials));
    dispatch(userLoginAsync());
  };

  return (
    <div className="page login-page">
      <ButtonControl onClick={() => login({ login: 'user@example.com', password: 'user' })}>
        Login
      </ButtonControl>
    </div>
  );
};
