import { useState, useEffect } from 'react';
import { ITask, INotification } from '../../../models/api/interfaces';
import { useSelector } from 'react-redux';
import { UserState } from '../../../models/api/user';

export const useMyProfilePageModel = () => {
  const user = useSelector((state: any) => state.user) as UserState;
  const [myTasks, setMyTasks] = useState<ITask[]>([]);
  const [othersTasks, setOthersTasks] = useState<ITask[]>([]);
  const [notifications, setNotifications] = useState<INotification[]>([]);
  const [isEditMode, setIsEditMode] = useState<boolean>(false);
  const tasks = useSelector((state: any) => state.tasks);

  useEffect(() => {
    setMyTasks(tasks.mine);
    setOthersTasks(tasks.others); //set only these, that I've applied to or was chosen to
    setNotifications([]);
  }, [tasks]);

  return { user, notifications, myTasks, othersTasks, isEditMode, setIsEditMode };
};
