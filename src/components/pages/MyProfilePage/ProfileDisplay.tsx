import React, { FunctionComponent } from 'react';
import { LabelControl } from '../../controls/LabelControl';
import { IType, IUser } from '../../../models/api/interfaces';
import { ContractorTypeControl } from '../../controls/ContractorTypeControl';
import './myProfilePage.scss';

export const ProfileDisplay: FunctionComponent<{ user: IUser }> = ({ user }) => {
  return (
    <>
      <LabelControl label="Nazwa" text={(user.info && user.info.name) || '-'} />
      <LabelControl label="Email" text={user.email} />
      <LabelControl label="Telefon" text={(user.info && user.info.phone) || '-'} />
      <LabelControl label="Typ" />
      {user.type && user.type.map((t: IType, i: any) => <ContractorTypeControl key={i} type={t} />)}
    </>
  );
};
