import React, { FunctionComponent, useState } from 'react';
import { LabelControl } from '../../controls/LabelControl';
import { IUser } from '../../../models/api/interfaces';
import './myProfilePage.scss';
import { InputControl } from '../../controls/InputControl';

export const ProfileEdit: FunctionComponent<{ user: IUser }> = ({ user }) => {
  const [newName, setNewName] = useState((user.info && user.info.name) || '');
  const [newEmail, setNewEmail] = useState(user.email);
  const [newPhone, setNewPhone] = useState((user.info && user.info.phone) || '');

  return (
    <>
      <LabelControl label="Nazwa" />
      <InputControl initialValue={newName} onChange={(e: any) => setNewName(e.target.value)} />

      <LabelControl label="Email" />
      <InputControl initialValue={newEmail} onChange={(e: any) => setNewEmail(e.target.value)} />

      <LabelControl label="Telefon" />
      <InputControl initialValue={newPhone} onChange={(e: any) => setNewPhone(e.target.value)} />

      {/*
        <LabelControl label="Typ" />
        { user.type && user.type.map((t: IType, i: any) => <ContractorTypeControl key={i} type={t} />)}
        */}
    </>
  );
};
