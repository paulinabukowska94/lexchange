import React, { FunctionComponent } from 'react';
import { useMyProfilePageModel } from './myProfilePage.state';
import { TasksBlock } from '../../blocks/TasksBlock/TasksBlock';
import { NotificationsBlock } from '../../blocks/NotificationsBlock/NotificationsBlock';
import { LabelControl } from '../../controls/LabelControl';
import { PageTitleControl } from '../../controls/PageTitleControl';
import { MyAccountIcon } from '../../controls/Icons';
import img from './task.png';
import { Avatar } from '@material-ui/core';
import './myProfilePage.scss';
import { ProfileDisplay } from './ProfileDisplay';
import { ProfileEdit } from './ProfileEdit';
import { ButtonControl } from '../../controls/ButtonControl';
import { SectionTitleControl } from '../../controls/SectionTitleControl';

export const MyProfilePage: FunctionComponent = () => {
  const { user, notifications, myTasks, isEditMode, setIsEditMode } = useMyProfilePageModel();

  return (
    <div className="page my-profile-page">
      <PageTitleControl text="Mój profil" />
      {user && user.user && (
        <div>
          <div className="user-panel">
            <Avatar alt="user" src={img} />
            <LabelControl text={user.user.email} startIcon={<MyAccountIcon />} />
          </div>
          <div className="info-panel">
            <div className="top">
              <SectionTitleControl text={isEditMode ? 'Edytuj dane' : 'Moje dane'} />
              <ButtonControl onClick={() => setIsEditMode(!isEditMode)}>
                {isEditMode ? 'Zapisz' : 'Edytuj'}
              </ButtonControl>
            </div>
            {isEditMode ? <ProfileEdit user={user.user} /> : <ProfileDisplay user={user.user} />}
          </div>
        </div>
      )}
      {/*
      <A href="/task/add">
        <LabelControl text="Dodaj zlecenie" startIcon={<AddTaskIcon/>} variant="h6"/>
      </A>
      */}
      <NotificationsBlock items={notifications} />
      <TasksBlock title="Moje zlecenia" items={myTasks} type="mine" />
    </div>
  );
};
