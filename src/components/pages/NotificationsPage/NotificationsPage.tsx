import React, { FunctionComponent } from 'react';
import { useNotificationsPageModel } from './notificationsPage.state';
import { INotification } from '../../../models/api/interfaces';
import { PageTitleControl } from '../../controls/PageTitleControl';
import { NotificationListItem } from '../../shared/NotificationListItem/NotificationListItem';
import { NoContentControl } from '../../controls/NoContentControl';
import { Spinner } from '../../shared/LayoutItems/Spinner';
import { BackButton } from '../../controls/BackButton';

export const NotificationsPage: FunctionComponent = () => {
  const { items } = useNotificationsPageModel();

  return (
    <div className="page notifications-page">
      <BackButton />
      <PageTitleControl text="Powiadomienia" />
      {items && items.length === 0 ? (
        <NoContentControl text="Brak powiadomień." />
      ) : items && items.length > 0 ? (
        <>
          {items && items.map((n: INotification) => <NotificationListItem key={n.id} item={n} />)}
        </>
      ) : (
        <Spinner />
      )}
    </div>
  );
};
