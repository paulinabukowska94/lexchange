import { INotification } from '../../../models/api/interfaces';

export const useNotificationsPageModel = () => {
  const items: INotification[] = [];
  return { items };
};
