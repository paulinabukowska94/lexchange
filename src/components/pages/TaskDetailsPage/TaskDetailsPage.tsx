import React, { FunctionComponent } from 'react';
import { useTaskDetailsPageModel } from './taskDetailsPage.state';
import { LabelControl } from '../../controls/LabelControl';
import { ModalControl } from '../../controls/ModalControl';
import { ButtonControl } from '../../controls/ButtonControl';
import { IApplication, IType } from '../../../models/api/interfaces';
import { ApplicationListItem } from '../../shared/ApplicationListItem/ApplicationListItem';
import { InputControl } from '../../controls/InputControl';
import { Spinner } from '../../shared/LayoutItems/Spinner';
import { UserAvatarAndNameControl } from '../../controls/UserAvatarAndNameControl';
import { ContractorTypeControl } from '../../controls/ContractorTypeControl';
import { TaskTypeControl } from '../../controls/TaskTypeControl';
import { PageTitleControl } from '../../controls/PageTitleControl';
import './taskDetailsPage.scss';
import { SectionTitleControl } from '../../controls/SectionTitleControl';
import { AddTaskIcon } from '../../controls/Icons';
import { BackButton } from '../../controls/BackButton';
import { ChosenApplicationListItem } from '../../shared/ChosenApplicationListItem/ChosenApplicationListItem';
import { NoContentControl } from '../../controls/NoContentControl';

export const TaskDetailsPage: FunctionComponent<{ taskId: any }> = ({ taskId }) => {
  const {
    task,
    showModal,
    setShowModal,
    application,
    setApplication,
    applyForTask,
    canUserApply,
    userCreatedThisTask,
    setAppicationAsChosen,
    visitProfile,
    deleteTask,
  } = useTaskDetailsPageModel(taskId);

  return (
    <div className="page task-details-page">
      <BackButton />
      {task ? (
        <div>
          <PageTitleControl text={task.title} />
          <div className="task-info-section">
            <UserAvatarAndNameControl
              title={task.paymaster.email}
              subtitle={task.paymaster.paymasterType}
            />

            {userCreatedThisTask() ? (
              <ButtonControl onClick={deleteTask}>Usuń</ButtonControl>
            ) : (
              <ButtonControl variant="outlined" onClick={() => visitProfile(task.paymaster.id)}>
                Zobacz profil
              </ButtonControl>
            )}

            {task.description && <LabelControl label="Opis" text={task.description} />}
            {task.place.id && <LabelControl label="Miejsce ID" text={task.place.id} />}
            {task.invoiceNeeded && (
              <LabelControl
                label="Czy potrzebna faktura"
                text={task.invoiceNeeded ? 'Tak' : 'Nie'}
              />
            )}
            {task.date && <LabelControl label="Data utworzenia" text={task.date} />}
            {task.price && <LabelControl label="Cena" text={task.price} />}
            {task.taskType && (
              <>
                <LabelControl label="Typ zlecenia" />
                <TaskTypeControl type={task.taskType} />
              </>
            )}

            {task.contractorType && (
              <>
                <LabelControl label="Akceptowane typy zleceniobiorcy:" />
                {task.contractorType.map((t: IType, i: any) => (
                  <ContractorTypeControl key={i} type={t} />
                ))}
              </>
            )}
          </div>

          {userCreatedThisTask() ? (
            <>
              {task.chosenApplication && (
                <div>
                  <SectionTitleControl startIcon={<AddTaskIcon />} text="Wybrane zgłoszenie:" />
                  <ChosenApplicationListItem
                    item={task.chosenApplication}
                    visitProfile={(userId: any) => visitProfile(userId)}
                  />
                </div>
              )}
              {task.applications && (
                <div>
                  <SectionTitleControl
                    startIcon={<AddTaskIcon />}
                    text="Lista wszystkich zgłoszeń"
                  />
                  {task.applications.length > 0 ? (
                    task.applications.map((a: IApplication) => (
                      <ApplicationListItem
                        item={a}
                        choose={(id: any) => setAppicationAsChosen(id)}
                        visitProfile={(userId: any) => visitProfile(userId)}
                      />
                    ))
                  ) : (
                    <NoContentControl text="Brak zgłoszeń do tego zlecenia." />
                  )}
                </div>
              )}
            </>
          ) : canUserApply() ? (
            <div className="apply-section">
              <SectionTitleControl startIcon={<AddTaskIcon />} text="Zgłoś się" />
              <div>
                <ButtonControl onClick={() => setShowModal(true)}>Aplikuj!</ButtonControl>
                <ModalControl show={showModal}>
                  <ButtonControl onClick={() => setShowModal(false)}>Zamknij</ButtonControl>
                  <LabelControl text="Aplikuj do zlecenia" variant="h5" />

                  <LabelControl text={`title: ${task.title}`} />
                  <LabelControl text={`desc: ${task.description}`} />
                  <LabelControl text={`place.id: ${task.place.id}`} />
                  <LabelControl text={`date:${task.date}`} />
                  <LabelControl text={`invoiceNeeded: ${task.invoiceNeeded}`} />
                  {task.price ? (
                    <LabelControl text={`price: ${task.price}`} />
                  ) : (
                    <div>
                      <LabelControl text="Zaproponuj swoją cenę" />
                      <InputControl
                        suffix="zł"
                        onChange={(e: any) =>
                          setApplication({ ...application, price: e.target.value })
                        }
                      />
                    </div>
                  )}
                  <LabelControl text={`taskType: ${task.taskType}`} />
                  <LabelControl
                    text={`contractorType: ${task.contractorType.map((c) => c)}`}
                    variant="h6"
                  />
                  <ButtonControl onClick={() => applyForTask()}>
                    Prześlij swoją aplikację
                  </ButtonControl>
                </ModalControl>
              </div>
            </div>
          ) : (
            <p>Nie masz dobrego typu zleceniobiorcy żeby aplikować do tego zlecenia.</p>
          )}
        </div>
      ) : (
        <Spinner />
      )}
    </div>
  );
};
