import { useState, useEffect } from 'react';
import { ITask, IApplication } from '../../../models/api/interfaces';
import { useSelector } from 'react-redux';
import Api from '../../../models/api/api';
import { navigate } from 'hookrouter';

export const useTaskDetailsPageModel = (taskId: number) => {
  const userState = useSelector((state: any) => state.user);

  const [task, setTask] = useState<ITask>();
  const [showModal, setShowModal] = useState<boolean>(false);
  const [application, setApplication] = useState<IApplication>({
    contractor: userState.user.id,
  } as IApplication);

  useEffect(() => {
    Api.getWithAuthorization(`tasks/task/${taskId}`).then((res: any) => {
      setTask(res.data);
    });
  }, [taskId]);

  const applyForTask = () => {
    setShowModal(false);
    setApplication({
      contractor: userState.user.id,
      price: (task && task.price) || application.price,
    } as IApplication);
    //let oldApplications = task.applications ? [...task.applications] : [] as IApplication[];
    //setTask({ ...task, applications: [oldApplications, application] as IApplication[]});
    //dispatch API call, dane powinny być w obiekcie `application`
  };

  const userCreatedThisTask = () => {
    return task && userState.user.id === task.paymaster.id;
  };

  const canUserApply = () => {
    let canApply = false;
    if (task) {
      let contractorTypeIds = task.contractorType.map((c: any) => c.id);
      for (let i = 0; i < userState.user.type.length; i++) {
        if (contractorTypeIds.includes(userState.user.type[i].id)) {
          canApply = true;
        }
      }
      return canApply;
    } else return false;
  };

  const setAppicationAsChosen = (id: any) => {
    //API CALL TO SET APPLICATION AS CHOSEN FOR THIS TASK
  };

  const visitProfile = (userId: any) => {
    navigate(`/profile/${userId}`);
  };

  const deleteTask = () => {
    Api.deleteWithAuthorization(`tasks/task/${taskId}`).then(() => {
      navigate(`/profile`);
    });
  };

  return {
    task,
    setTask,
    showModal,
    setShowModal,
    application,
    setApplication,
    applyForTask,
    canUserApply,
    userCreatedThisTask,
    setAppicationAsChosen,
    visitProfile,
    deleteTask,
  };
};
