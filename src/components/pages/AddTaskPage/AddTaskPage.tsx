import React, { FunctionComponent, useState } from 'react';
import { useAddTaskPageModel } from './addTaskPage.state';
import { places } from './../../../models/data/places';
import { contractorTypes } from '../../../models/data/contractorTypes';
import { DateControl } from '../../controls/DateControl';
import { SelectControl } from '../../controls/SelectControl';
import { LabelControl } from '../../controls/LabelControl';
import { InputControl } from '../../controls/InputControl';
import { SwitchControl } from '../../controls/SwitchControl';
import { taskTypes } from '../../../models/data/taskTypes';
import { ButtonControl } from '../../controls/ButtonControl';
import { PageTitleControl } from '../../controls/PageTitleControl';
import { RHFInput } from 'react-hook-form-input';
import useForm from 'react-hook-form';
import moment from 'moment';
import { ConfirmationModalControl } from '../../controls/ConfirmationModalControl';

export const AddTaskPage: FunctionComponent = () => {
  const {
    onSubmit,
    showConfirmationModal,
    closeConfirmationModal,
    confirmationModalMessage,
  } = useAddTaskPageModel();
  const { handleSubmit, register, errors, setValue } = useForm();
  const [date, setDate] = useState(moment());

  return (
    <div className="page add-task-page">
      <PageTitleControl text="Dodaj zlecenie" />
      <form onSubmit={handleSubmit(onSubmit)} noValidate>
        <LabelControl text="Tytuł zlecenia" />
        <InputControl
          name="title"
          error={!!errors.title}
          inputRef={register({ pattern: /^(?!\s*$).+/ })}
        />
        <p className="error">{errors.title && 'To pole nie może być puste'}</p>

        <LabelControl text="Opis zlecenia" />
        <InputControl name="description" multiline inputRef={register} />

        <LabelControl text="Miejsce" />
        <RHFInput
          name="place"
          as={
            <SelectControl
              options={places.map((p) => ({ value: p.id, label: p.info.name }))}
              placeholder="Wybierz miejsce..."
              noOptionsMessage={() => 'Brak dopasowań'}
            />
          }
          rules={{ required: true }}
          register={register}
          setValue={setValue}
        />
        <p className="error">{errors.contractorType && 'To pole nie może być puste'}</p>

        <LabelControl text="Data (opcjonalnie)" />
        <RHFInput
          name="taskDate"
          as={
            <DateControl id="taskDate" date={date} onChange={(e: any) => setDate(e.target.value)} />
          }
          register={register}
          setValue={setValue}
        />

        <LabelControl text="Zleceniobiorca" />
        <RHFInput
          name="contractorType"
          as={
            <SelectControl
              options={contractorTypes.map((c) => ({ value: c.id, label: c.name }))}
              isMulti
              placeholder="Wybierz rodzaj zleceniobiorcy..."
              noOptionsMessage={() => 'Brak dopasowań'}
            />
          }
          register={register}
          setValue={setValue}
        />

        <LabelControl text="Czy potrzebna faktura?" />
        <RHFInput
          name="invoiceNeeded"
          as={
            <SwitchControl
              checked={false}
              options={[
                { id: 0, name: 'Nie' },
                { id: 1, name: 'Tak' },
              ]}
            />
          }
          register={register}
          setValue={setValue}
        />

        <LabelControl text="Cena" />
        <LabelControl text="Jeśli nie podasz ceny, inni użytkownicy aplikujący do twojego zlecenia będą mogli ją zaproponować." />
        <InputControl name="price" type="number" suffix="zł" inputRef={register} />

        <LabelControl text="Rodzaj zlecenia" />
        <RHFInput
          name="taskType"
          as={<SwitchControl checked={false} options={taskTypes} />}
          register={register}
          setValue={setValue}
        />

        <div>
          <ButtonControl variant="contained" type="submit" className="primary">
            Dodaj
          </ButtonControl>
        </div>

        <ConfirmationModalControl show={showConfirmationModal} onClose={closeConfirmationModal}>
          <p>{confirmationModalMessage}</p>
          <ButtonControl onClick={() => closeConfirmationModal()}></ButtonControl>
        </ConfirmationModalControl>
      </form>
    </div>
  );
};
