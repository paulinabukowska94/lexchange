import { useState } from 'react';
import Api from '../../../models/api/api';
import { useSelector } from 'react-redux';
import { navigate } from 'hookrouter';

export const useAddTaskPageModel = () => {
  const userState = useSelector((state: any) => state.user);
  const [showConfirmationModal, setShowConfirmationModal] = useState(false);
  const [confirmationModalMessage, setConfirmationModalMessage] = useState('');

  const onSubmit = (values: any, e: any) => {
    e.preventDefault();
    let contractorTypes = values.contractorType.map((ct: any) => ct.value);
    let newTask = {
      title: values.title,
      date: new Date(values.taskDate),
      description: values.description || '',
      invoiceNeeded: values.invoiceNeeded,
      active: true,
      price: values.price || null,
      paymaster: userState.user.id,
      place: values.place ? values.place.value : null,
      contractorType: contractorTypes,
      taskType: values.taskType ? 0 : 1,
    };

    Api.postWithAuthorization(`tasks/tasks`, newTask)
      .then((res: any) => {
        navigate(`/task/${res.data.id}`);
      })
      .catch(() => {
        setShowConfirmationModal(true);
        setConfirmationModalMessage('Wystąpił błąd.');
      });
  };

  const closeConfirmationModal = () => {
    setShowConfirmationModal(false);
  };

  return { onSubmit, confirmationModalMessage, showConfirmationModal, closeConfirmationModal };
};
