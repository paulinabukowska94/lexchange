import React, { FunctionComponent } from 'react';
import { useAuthorizationPageModel } from './authorizationPage.state';
import { ButtonControl } from '../../controls/ButtonControl';
import { InputControl } from '../../controls/InputControl';
import { ArrowRightAltOutlined } from '@material-ui/icons';
import { LabelControl } from '../../controls/LabelControl';
import './authorizationPage.scss';

export const AuthorizationPage: FunctionComponent = () => {
  const {
    isLoginPage,
    setIsLoginPage,
    login,
    register,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    email,
    setEmail,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    password,
    setPassword,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    user,
  } = useAuthorizationPageModel();

  return (
    <div className="page authorization-page">
      <LabelControl text={isLoginPage ? 'Zaloguj się' : 'Zarejestruj się'} variant="h5" />
      <LabelControl text="Email" />
      <InputControl onChange={(e: any) => setEmail(e.target.value)} />
      <LabelControl text="Hasło" />
      <InputControl onChange={(e: any) => setPassword(e.target.value)} />
      <ButtonControl
        endIcon={<ArrowRightAltOutlined />}
        fullWidth
        // FIXME: needs smarter autologin for dev
        onClick={() => {
          if (isLoginPage) {
            login({ login: 'user@example.com', password: 'user' }); //login({ login: email, password: password})
          } else {
            register({ login: 'user@example.com', password: 'user' }); //register({ login: email, password: password})
          }
        }}
      >
        {isLoginPage ? 'Zaloguj się' : 'Zarejestruj się'}
      </ButtonControl>
      {isLoginPage ? (
        <ButtonControl fullWidth variant="outlined" onClick={() => setIsLoginPage(false)}>
          Nie masz konta? Zarejestruj się
        </ButtonControl>
      ) : (
        <ButtonControl fullWidth variant="outlined" onClick={() => setIsLoginPage(true)}>
          Masz już konto? Zaloguj się
        </ButtonControl>
      )}
    </div>
  );
};
