import { UserState } from './../../../models/api/user';
import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  userLoginAsync,
  userRegisterAsync,
  userFillCredentials,
} from '../../../redux/actions/user';
import { Credentials } from '../../../models/api/user';

export const useAuthorizationPageModel = () => {
  const [isLoginPage, setIsLoginPage] = useState(true);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const user = useSelector((state: any) => state.user) as UserState;
  const dispatch = useDispatch();

  const login = (credentials: Credentials) => {
    dispatch(userFillCredentials(credentials));
    dispatch(userLoginAsync());
  };

  const register = (credentials: Credentials) => {
    dispatch(userFillCredentials(credentials));
    dispatch(userRegisterAsync()); //dodać obsługę sukcesu i błędu
    setIsLoginPage(true);
  };

  // DEBUG
  // FIXME: needs smarter auto login for dev
  useEffect(() => {
    login({ login: 'user@example.com', password: 'user' });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return {
    isLoginPage,
    setIsLoginPage,
    login,
    register,
    email,
    setEmail,
    password,
    setPassword,
    user,
  };
};
