import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Api from './models/api/api';
import { useRoutes } from 'hookrouter';
import routes from './router';
import React from 'react';
import { useMediaQuery } from 'react-responsive';
import { tasksFetchAsync } from './redux/actions/tasks';

export const useAppModel = () => {
  Api.init();
  const routeResult = useRoutes(routes);
  const [value, setValue] = React.useState(0);
  const dispatch = useDispatch();
  const user = useSelector((state: any) => state.user);

  useEffect(() => {
    dispatch(tasksFetchAsync());
  }, [dispatch, user.token]);

  const isTabletOrMobile = useMediaQuery({ maxWidth: 1224 });
  const isDesktopOrLaptop = useMediaQuery({ minWidth: 1224 });

  return { user, routeResult, value, setValue, isTabletOrMobile, isDesktopOrLaptop };
};
