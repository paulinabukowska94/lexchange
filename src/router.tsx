import React from 'react';
import { HomePage } from './components/pages/HomePage/HomePage';
import { TaskDetailsPage } from './components/pages/TaskDetailsPage/TaskDetailsPage';
import { AddTaskPage } from './components/pages/AddTaskPage/AddTaskPage';
import { UserProfilePage } from './components/pages/UserProfilePage/UserProfilePage';
import { TasksPage } from './components/pages/TasksPage/TasksPage';
import { NotificationsPage } from './components/pages/NotificationsPage/NotificationsPage';
import { MyProfilePage } from './components/pages/MyProfilePage/MyProfilePage';

const routes = {
  '/': () => <HomePage />,
  '/task/add': () => <AddTaskPage />,
  '/profile': () => <MyProfilePage />,
  '/profile/:id': ({ id }: any) => <UserProfilePage id={id} />,
  '/task/:id': ({ id }: any) => <TaskDetailsPage taskId={id} />,
  '/notifications': () => <NotificationsPage />,
  '/tasks': () => <TasksPage type={'others'} />,
  '/tasks/mine': () => <TasksPage type={'mine'} />,
  '/tasks/all': () => <TasksPage type={'all'} />,
  '/tasks/others': () => <TasksPage type={'others'} />,
};

export default routes;
