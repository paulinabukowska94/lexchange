import React from 'react';
import {
  BottomNavigation,
  BottomNavigationAction,
  Grid,
  Container,
  Divider,
  ListItem,
  ListItemIcon,
  List,
  ListItemText,
  Paper,
} from '@material-ui/core';
import { AuthorizationPage } from './components/pages/AuthorizationPage/AuthorizationPage';
import { useAppModel } from './app.state';
import { ThemeProvider } from '@material-ui/styles';
import theme from './theme/Theme';
import { LayoutLogo } from './components/shared/LayoutItems/LayoutLogo';
import { A } from 'hookrouter';
import './scss/app.scss';
import {
  HomeIcon,
  NotificationIcon,
  MyAccountIcon,
  TasksIcon,
  AddTaskIcon,
} from './components/controls/Icons';
import { UserAvatarAndNameControl } from './components/controls/UserAvatarAndNameControl';

function App() {
  const { user, routeResult, value, setValue, isTabletOrMobile, isDesktopOrLaptop } = useAppModel();

  const menu = [
    {
      name: 'Start',
      path: '/',
      icon: <HomeIcon />,
    },
    {
      name: 'Powiadomienia',
      path: '/notifications',
      icon: <NotificationIcon />,
    },
    {
      name: 'Mój profil',
      path: '/profile',
      icon: <MyAccountIcon />,
    },
    {
      name: 'Zlecenia',
      path: '/tasks',
      icon: <TasksIcon />,
    },
    {
      divider: true,
    },
    {
      name: 'Dodaj zlecenie',
      path: '/task/add',
      icon: <AddTaskIcon />,
    },
  ];

  return (
    <ThemeProvider theme={theme}>
      <div className="app">
        {!user.token ? (
          <AuthorizationPage />
        ) : (
          <>
            <Container maxWidth="lg">
              <Grid container spacing={3}>
                <Grid item xs={12} lg={3}>
                  <div className="header">
                    <LayoutLogo />
                    {isDesktopOrLaptop && (
                      <div className="items">
                        <UserAvatarAndNameControl title={user.user.email} subtitle={user.user.id} />
                        <List component="nav" aria-label="main">
                          {menu.map((el, i) =>
                            el.divider ? (
                              <Divider />
                            ) : (
                              <ListItem
                                key={i}
                                button
                                component={(props) => <A href={el.path} {...props} />}
                                href={el.path}
                              >
                                <ListItemIcon>{el.icon ? el.icon : <TasksIcon />}</ListItemIcon>
                                <ListItemText primary={el.name} />
                              </ListItem>
                            ),
                          )}
                        </List>
                      </div>
                    )}
                  </div>
                </Grid>
                <Grid item xs={12} lg={9}>
                  {routeResult}
                </Grid>
              </Grid>
            </Container>
            {isTabletOrMobile && (
              <footer className="bottomNav">
                <Paper>
                  <BottomNavigation
                    value={value}
                    onChange={(event, newValue) => {
                      setValue(newValue);
                    }}
                    classes={{ root: 'navigation' }}
                    showLabels={true}
                  >
                    <BottomNavigationAction
                      component={A}
                      href="/"
                      label="Start"
                      icon={<HomeIcon />}
                      showLabel={true}
                    />
                    <BottomNavigationAction
                      component={A}
                      href="/notifications"
                      label="Powiadomienia"
                      icon={<NotificationIcon />}
                      showLabel={true}
                    />
                    <BottomNavigationAction
                      component={A}
                      href="/profile"
                      label="Mój profil"
                      icon={<MyAccountIcon />}
                      showLabel={true}
                    />
                    <BottomNavigationAction
                      component={A}
                      href="/tasks"
                      label="Zlecenia"
                      icon={<TasksIcon />}
                      showLabel={true}
                    />
                  </BottomNavigation>
                </Paper>
              </footer>
            )}
          </>
        )}
      </div>
    </ThemeProvider>
  );
}

export default App;
