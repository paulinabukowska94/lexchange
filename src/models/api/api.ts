import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';
import { useSelector } from 'react-redux';

export default class Api {
  private static axiosInstance: AxiosInstance;
  private static user: any;
  public static base: string;

  static init() {
    this.base = 'https://lexchange.dev.robierzeczy.pl/api/';
    this.user = useSelector((state: any) => state.user);
    this.axiosInstance = axios.create({
      baseURL: this.base,
    });
  }

  static get<T>(url: string, headers?: any) {
    return this.axiosInstance.get<T>(url, headers);
  }

  static post(url: string, data?: any, config?: AxiosRequestConfig) {
    return this.axiosInstance.post(url, data, config);
  }

  static put(url: string, data?: any, config?: AxiosRequestConfig) {
    return this.axiosInstance.put(url, data, config);
  }

  static delete(url: string, config?: AxiosRequestConfig) {
    return this.axiosInstance.delete(url, config);
  }

  static getWithAuthorization(url: string) {
    return this.get(url, { headers: { Authorization: `Bearer ${this.user.token}` } });
  }

  static postWithAuthorization(url: string, data: any, additionalHeaders?: any) {
    if (additionalHeaders) {
      let headers = {
        Authorization: `Bearer ${this.user.token}`,
        ...additionalHeaders,
      };
      return this.post(url, data, { headers: headers });
    } else {
      return this.post(url, data, { headers: { Authorization: `Bearer ${this.user.token}` } });
    }
  }

  static putWithAuthorization(url: string, data: any) {
    return this.put(url, data, { headers: { Authorization: `Bearer ${this.user.token}` } });
  }

  static deleteWithAuthorization(url: string) {
    return this.delete(url, { headers: { Authorization: `Bearer ${this.user.token}` } });
  }
}
