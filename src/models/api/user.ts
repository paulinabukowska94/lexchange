import { IUser } from './interfaces';

export interface Credentials {
  login: String;
  password: String;
}

export interface UserState {
  credentials: Credentials;
  token: string | null;
  user: IUser | null;
  isLoading: boolean;
  loginError: string | null;
}
