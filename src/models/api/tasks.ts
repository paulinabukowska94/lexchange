import { ITask } from './interfaces';

export interface TasksState {
  mine: ITask[] | null;
  others: ITask[] | null;
  all: ITask[] | null;
  isLoading: boolean;
  error: string | null;
}
