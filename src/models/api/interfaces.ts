export interface IUser {
  id?: number;
  email: string;
  info: IContactInfo;
  issuesInvoice?: boolean; //czy wystawia fakturę
  type?: IType[]; //jesli ktoś chce TYLKO wystawiać ogłoszenia, nie musi podawać
  paymasterType?: any; // indywidualny adwokat/indywidualny radca/mała kancelaria/kancelaria/korporacja //PaymasterType
  //znajomosc jezykow
  //wiek
  //zdjecie
  //liczba zrealizowanych zleceń
  //w jakiej izbie jest zrzeszony
}

export interface IPlace {
  id?: number;
  lat: number;
  lng: number;
  info: IContactInfo;
}

export interface ITask {
  //zlecenie
  id?: number;
  active: boolean;
  title: string;
  paymaster: IUser;
  place: IPlace;
  date: Date;
  description?: string;
  invoiceNeeded?: boolean;
  price?: number;
  taskType: IType;
  contractorType: IType[];
  applications?: IApplication[];
  chosenApplication?: IApplication;
}

export interface IApplication {
  id?: number;
  contractor: IUser;
  price?: number;
}

export interface INotification {
  id?: number;
  task: ITask;
  message: string;
  type: IType;
}

export interface IContactInfo {
  name?: string;
  phone?: string;
  email?: string;
}

export interface IType {
  id: number;
  name: string;
} //
