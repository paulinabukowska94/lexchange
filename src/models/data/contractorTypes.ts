import { IType } from './../api/interfaces';

export const contractorTypes: IType[] = [
  {
    id: 0,
    name: 'aplikant adwokacki',
  },
  {
    id: 1,
    name: 'adwokat',
  },
  {
    id: 2,
    name: 'aplikant radcowski',
  },
  {
    id: 3,
    name: 'radca prawny',
  },
];
