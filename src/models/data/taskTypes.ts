import { IType } from '../api/interfaces';

export const taskTypes: IType[] = [
  {
    id: 0,
    name: 'Zlecenie',
  },
  {
    id: 1,
    name: 'Zastępstwo',
  },
];
