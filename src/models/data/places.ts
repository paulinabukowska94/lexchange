import { IPlace } from '../api/interfaces';

export const places: IPlace[] = [
  {
    id: 0,
    lat: 0,
    lng: 0,
    info: {
      name: 'Sąd rejonowy w Gdańsku',
      phone: '+58 303 400',
      email: 'email@email.com',
    },
  },
  {
    id: 2,
    lat: 0,
    lng: 0,
    info: {
      name: 'Sąd okręgowy w Warszawie',
      phone: '+58 303 400',
      email: 'email@email.com',
    },
  },
];
