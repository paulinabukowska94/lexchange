import { createMuiTheme } from '@material-ui/core';
import '@material-ui/core/colors/';

export default createMuiTheme({
  palette: {
    primary: {
      main: '#5b7de3',
    },
    secondary: {
      main: '#3dae8d',
    },
    text: {
      primary: '#333333',
      secondary: '#00000',
    },
  },
  props: {
    MuiButtonBase: {
      disableRipple: true,
    },
  },
  typography: {
    fontSize: 12,
  },
});
