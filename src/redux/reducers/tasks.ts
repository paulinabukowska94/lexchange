import { TasksState } from '../../models/api/tasks';

const tasksEmpty: TasksState = {
  mine: null,
  others: null,
  all: null,
  isLoading: true,
  error: '',
};

export const tasks = (state = tasksEmpty, action: any) => {
  switch (action.type) {
    case 'TASKS_FETCH': {
      return {
        ...state,
        mine: action.mine,
        others: action.others,
        all: action.all,
        isLoading: false,
        error: action.error,
      };
    }
    default:
      return state;
  }
};
