import { Credentials, UserState } from './../../models/api/user';

const userEmpty = {
  credentials: {} as Credentials,
  token: null,
  user: { id: 0 },
  isLoading: false,
  loginError: '',
} as UserState;

export const user = (state = userEmpty, action: any) => {
  switch (action.type) {
    case 'USER_FILL_CREDENTIALS': {
      return {
        ...state,
        credentials: action.credentials,
        isLoading: true,
      };
    }
    case 'USER_LOGIN': {
      return {
        ...state,
        token: action.token,
        user: action.user,
        isLoading: false,
        loginError: action.loginError,
      };
    }
    case 'USER_REGISTER': {
      return state;
    }
    case 'USER_FETCH': {
      return state;
    }
    default:
      return state;
  }
};
