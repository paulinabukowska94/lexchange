import Api from '../../models/api/api';

export const tasksFetch = (mine: any, others: any, all: any, error: any) => ({
  type: 'TASKS_FETCH',
  mine,
  others,
  all,
  error,
});

export const tasksFetchAsync = () => (dispatch: any, getState: any) => {
  const user = getState().user;
  if (getState().user.token) {
    const headers = { headers: { Authorization: `Bearer ${user.token}` } };
    Api.get('tasks/tasks?scope=mine', headers)
      .then((res: any) => {
        Api.get('tasks/tasks?scope=others', headers)
          .then((res2: any) => {
            Api.get('tasks/tasks?scope=all', headers)
              .then((res3: any) => {
                dispatch(tasksFetch(res.data, res2.data, res3.data, null));
              })
              .catch((err: any) => {
                dispatch(tasksFetch([], [], [], err));
              });
          })
          .catch((err: any) => {
            dispatch(tasksFetch([], [], [], err));
          });
      })
      .catch((err: any) => {
        dispatch(tasksFetch([], [], [], err));
      });
  }
};
