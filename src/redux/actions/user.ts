import { IUser } from './../../models/api/interfaces';
import { Credentials } from '../../models/api/user';
import Api from '../../models/api/api';

export const userFillCredentials = (credentials?: Credentials) => ({
  type: 'USER_FILL_CREDENTIALS',
  credentials,
});

export const userLogin = (token: string | null, user: IUser | null, loginError: any) => ({
  type: 'USER_LOGIN',
  token,
  user,
  loginError,
});

export const userLoginAsync = () => (dispatch: any, getState: any) => {
  Api.post('login_check', {
    username: getState().user.credentials.login,
    password: getState().user.credentials.password,
  })
    .then((res) => {
      Api.get('user/profile', { headers: { Authorization: `Bearer ${res.data.token}` } }).then(
        (res2: any) => {
          dispatch(userLogin(res.data.token, res2.data, null));
        },
      );
    })
    .catch((err) => {
      dispatch(userLogin(null, null, err));
    });
};

export const userRegister = (registerError: any) => ({
  type: 'USER_REGISTER',
  registerError,
});

export const userRegisterAsync = () => (dispatch: any, getState: any) => {
  Api.post('register', {
    username: getState().user.credentials.login,
    password: getState().user.credentials.password,
  })
    .then((res) => {
      dispatch(userRegister(null));
    })
    .catch((err) => {
      dispatch(userRegister(err));
    });
};
